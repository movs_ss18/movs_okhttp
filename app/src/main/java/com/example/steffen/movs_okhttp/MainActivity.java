package com.example.steffen.movs_okhttp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;



import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    Button bt_sync, bt_async;
    TextView tv_content;
    Switch sw_name;
    EditText et_firstName, et_lastName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sw_name = (Switch) findViewById(R.id.sw_name);
        et_firstName = (EditText) findViewById(R.id.et_firstName);
        et_lastName = (EditText) findViewById(R.id.et_lastName);
        tv_content = (TextView) findViewById(R.id.tv_content);
        bt_sync = (Button) findViewById(R.id.bt_sync);
        bt_async = (Button) findViewById(R.id.bt_async);

        bt_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewChuck(true);
            }
        });

        bt_async.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewChuck(false);
            }
        });

    }


    public void loadNewChuck(Boolean sync){
        Request req = buildRequest();
        if (sync){
            LoadData myTask = new LoadData();
            myTask.execute(req);
        } else {
            sendAsyncRequest(req);
        }
    }


    public Request buildRequest(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse("http://api.icndb.com/jokes/random").newBuilder();

        if (sw_name.isChecked()) {
            String firstName = et_firstName.getText().toString();
            String lastName = et_lastName.getText().toString();
            urlBuilder.addQueryParameter("firstName", firstName);
            urlBuilder.addQueryParameter("lastName", lastName);
        }
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();
        return request;
    }


    public void sendAsyncRequest(Request request){

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    //tv_content.setText("Response failed");
                    throw new IOException("Unexpected code " + response);
                } else {
                    // do something wih the result
                    final String responseS = response.body().string();
                    parseResponse(responseS);
                }
            }


        });
    }

    public String sendSnycRequest(Request request){
        String output = "";
        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            output = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }


    private class LoadData extends AsyncTask<Request, Void, String> {

        @Override
        protected String doInBackground(Request... params) {
            String output = sendSnycRequest(params[0]);
            return output;
        }

        @Override
        protected void onPostExecute(String result) {
            parseResponse(result);
        }
    }


    public void parseResponse(final String responseS) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(responseS);
                    tv_content.setText(Jobject.getJSONObject("value").getString("joke"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
